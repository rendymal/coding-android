// import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

// (Latihan Text Widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     var scaffold = Scaffold(
//       appBar: AppBar(
//         title: Text("Aplikasi Hello World"),
//       ),
//       body: Center(
//           child: Container(
//               color: Colors.lightBlue,
//               width: 150,
//               height: 80,
//               child: Text(
//                 "Rendy Malikulmulki Wahid",
//                 textAlign: TextAlign.center,
//                 style: TextStyle(
//                     color: Colors.white,
//                     fontStyle: FontStyle.italic,
//                     fontWeight: FontWeight.w700,
//                     fontSize: 20),
//               ))),
//     );
//     return MaterialApp(
//       home: scaffold,
//     );
//   }
// }

// (Latihan Row dan Colom)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home : Scaffold(
//         appBar: AppBar(title: Text("Latihan Row dan Colom"),),
//         body: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Text("Text 1"),
//             Text("Text 2"),
//             Text("Text 3"),
//             Row(
//               children: <Widget>[
//                 Text("Text 4"),
//                 Text("Text 5"),
//                 Text("Text 6")
//               ]
//             )
//           ]
//         ),
//       ),
//     );
//   }
// }

// (Latihan Container)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan Container"),
//         ),
//         body: Container(
//           color: Colors.red,
//           margin: EdgeInsets.fromLTRB(10, 15, 20, 25),
//           padding: EdgeInsets.only(
//             bottom: 20,
//             top: 20,
//           ),
//           child: Container(
//             margin: EdgeInsets.all(10),
//             decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(50),
//                 gradient: LinearGradient(
//                     begin: Alignment.topLeft,
//                     colors: <Color>[Colors.amber, Colors.blue])),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latiahan stateless dan statefull)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }
// class _MyAppState extends State<MyApp> {
//   int number = 0;
//   void tekanTombol() {
//     setState(() {
//       number = number + 1;
//     });
//   }
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Statefull Widgets Demo"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(number.toString(), style: TextStyle(fontSize: 10 + number.toDouble()),),
//               RaisedButton(
//                 child: Text("Tambah Bilangan"),
//                 onPressed: tekanTombol,
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan Anonymous Method)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   String message = "ini adalah text";
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Anonymous Method"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(message),
//               RaisedButton(
//                 child: Text("Tekan saya"),
//                 onPressed: () {
//                   setState(() {
//                     message = "tombol sudah ditekan";
//                   });
//                 }, // memanggil method langsung tanpa nama method
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan Text Style)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan Text Style"),
//         ),
//         body: Center(
//           child: Text(
//             "ini adalah text",
//             style: TextStyle(
//               fontStyle: FontStyle.italic,
//               fontSize: 50,
//               decoration: TextDecoration.underline,
//               decorationColor: Colors.red,
//               decorationThickness: 5,
//               decorationStyle: TextDecorationStyle.wavy
//           ),
//         ),
//       ),
//     ),
//     );
//   }
// }

// (Latihan list dan listview)
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
class _MyAppState extends State<MyApp> {
  List<Widget> widgets = [];
  int counter = 1;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan list dan listview"),
        ),
        body: ListView(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                // ignore: deprecated_member_use
                RaisedButton(
                  child: Text("Tambah Data"),
                  onPressed: () {
                    setState(() {
                      widgets.add(Text(
                        "Data Ke " + counter.toString(),
                        style: TextStyle(fontSize: 35),
                      ));
                      counter++;
                    });
                  },
                ),
                // ignore: deprecated_member_use
                RaisedButton(
                  child: Text("Hapus Data"),
                  onPressed: (){
                    setState(() {
                      widgets.removeLast();
                      counter--;
                    });
                  },
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets,
            )
          ],
        ),
      ),
    );
  }
}

// (Latihan AnimatedContainer dan GestureDetektor)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   Random random =
//       Random(); // untuk memasang bilangan random, maka diperlukan import dath:math di awal file
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan AnimatedContainer dan GestureDetektor"),
//         ),
//         body: Center(
//           child: GestureDetector(
//             onTap: () {
//               setState(() {});
//             },
//             child: AnimatedContainer(
//               color: Color.fromARGB(255, random.nextInt(256),
//                   random.nextInt(256), random.nextInt(256)),
//               duration: Duration(seconds: 1),
//               width: 50.0 + random.nextInt(101),
//               // agar pengaturan lebarnya antara 50 sampai 100, dan di tulis 101 karena 101 tidak akan pernah dijalankan
//               height: 50.0 + random.nextInt(101),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
