import 'package:flutter/material.dart';
// import 'package:flutter_application_41sampai50/application_color.dart';
// import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart'; // shared preferences

void main() {
  runApp(MyApp());
}

// (Latihan 41 => shared Preferences dan Double Question Mark Operator)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   TextEditingController controller = TextEditingController(text: "No Name");
//   bool isOn = false;

//   void saveData() async {
//     SharedPreferences pref = await SharedPreferences.getInstance();
//     pref.setString("nama", controller.text);
//     pref.setBool("isOn", isOn);
//   }

//   Future<String> getNama() async {
//     SharedPreferences pref = await SharedPreferences.getInstance();
//     return pref.getString("nama") ?? "No Name";
//     // arti dari tanda tanya 2 => jika pref.getString("nama") = Null (dalam artian belum pernah save nama) maka return "No Name"
//   }

//   Future<bool> getOn() async {
//     SharedPreferences pref = await SharedPreferences.getInstance();
//     return pref.getBool("isOn") ?? false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Shared Preferences"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               TextField(
//                 controller: controller,
//               ),
//               Switch(
//                   value: isOn,
//                   onChanged: (newValue) {
//                     setState(() {
//                       isOn = newValue;
//                     });
//                   }),
//               // ignore: deprecated_member_use
//               RaisedButton(
//                 onPressed: () {
//                   saveData();
//                 },
//                 child: Text("Save"),
//               ),
//               // ignore: deprecated_member_use
//               RaisedButton(
//                 onPressed: () {
//                   getNama().then((s) {
//                     controller.text = s;
//                     setState(() {});
//                   });
//                   getOn().then((b) {
//                     isOn = b;
//                     setState(() {});
//                   });
//                 },
//                 child: Text("Load"),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 42 => Provider State Management)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: ChangeNotifierProvider<ApplicationColor>(
//         create: (context) => ApplicationColor(),
//         // builder: (context) => ApplicationColor(),
//         child: Scaffold(
//           appBar: AppBar(
//             backgroundColor: Colors.black,
//             title: Consumer<ApplicationColor>(
//               builder: (context, applicationColor, _) => Text(
//                 "contoh provider",
//                 style: TextStyle(color: applicationColor.color),
//               ),
//             ),
//           ),
//           body: Center(
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Consumer<ApplicationColor>(
//                   builder: (context, applicationColor, _) => AnimatedContainer(
//                     margin: EdgeInsets.all(5),
//                     duration: Duration(milliseconds: 500),
//                     width: 100,
//                     height: 100,
//                     color: applicationColor.color,
//                   ),
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Container(margin: EdgeInsets.all(5), child: Text("Yellow")),
//                     Consumer<ApplicationColor>(
//                       builder: (context, applicationColor, _) => Switch(
//                         value: applicationColor.isLightBlue,
//                         onChanged: (newValue) {
//                           applicationColor.isLightBlue = newValue;
//                         },
//                       ),
//                     ),
//                     Container(margin: EdgeInsets.all(5), child: Text("Blue"))
//                   ],
//                 )
//               ],
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(appBar: AppBar(title: Text("Uji Coba kedua"),),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              color: Colors.amber,
            )
          ],
        ),
      ),),
    );
  }
}
