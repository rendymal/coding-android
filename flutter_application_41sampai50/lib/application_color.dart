import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ApplicationColor with ChangeNotifier{
  bool _isLightBlue = true;

  // ignore: unnecessary_getters_setters
  bool get isLightBlue => _isLightBlue;
  // ignore: unnecessary_getters_setters
  set isLightBlue(bool value) {
    _isLightBlue = value;
    notifyListeners();
  }

  Color get color => (_isLightBlue) ? Colors.lightBlue : Colors.amber;
}