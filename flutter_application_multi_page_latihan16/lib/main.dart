import 'package:flutter/material.dart';
import 'package:flutter_application_multi_page_latihan16/login_page.dart';

void main() {
  runApp(MyApp());
}

// (Latihan Navigasi multi page / screen)
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginPage(),
    );
  }
}