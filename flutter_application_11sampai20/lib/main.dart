import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

// (Latihan Flexible widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Flexible Layout"),
//         ),
//         body: Column(
//           children: <Widget>[
//             Flexible(
//               flex: 1,
//               child: Row(
//                 children: <Widget>[
//                   Flexible(
//                   flex: 1,
//                   child: Container(
//                     margin: EdgeInsets.all(5),
//                     color: Colors.red,
//                   )),
//                   Flexible(
//                   flex: 1,
//                   child: Container(
//                     margin: EdgeInsets.all(5),
//                     color: Colors.green,
//                   )),
//                   Flexible(
//                   flex: 1,
//                   child: Container(
//                     margin: EdgeInsets.all(5),
//                     color: Colors.purple,
//                   )),
//                 ],
//               ),
//             ),
//             Flexible(
//                 flex: 2,
//                 child: Container(
//                   margin: EdgeInsets.all(5),
//                   color: Colors.amber,
//                 )),
//             Flexible(
//                 flex: 1,
//                 child: Container(

//                   color: Colors.blue,
//                 )),
//           ],
//         ),
//       ),
//     );
//   }
// }

// (Latihan stack dan align widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan Stack dan Align"),
//         ),
//         body: Stack(
//           children: <Widget>[
//             // Background
//             Column(
//               children: <Widget>[
//                 Flexible(
//                   flex: 1,
//                   child: Row(
//                     children: <Widget>[
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.white,
//                         ),
//                       ),
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.black12,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Flexible(
//                   flex: 1,
//                   child: Row(
//                     children: <Widget>[
//                       Flexible(
//                         flex: 1,
//                         child: Container(color: Colors.black12),
//                       ),
//                       Flexible(
//                         flex: 1,
//                         child: Container(
//                           color: Colors.white,
//                         ),
//                       ),
//                     ],
//                   ),
//                 )
//               ],
//             ),
//             // Listview dengan text
//             ListView(
//               children: <Widget>[
//                 Column(
//                   children: <Widget>[
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                     Container(
//                         margin: EdgeInsets.all(10),
//                         child: Text(
//                             "ini adalah text yang ada di lapisan tengah dari stack",
//                             style: TextStyle(fontSize: 30))),
//                   ],
//                 )
//               ],
//             ),
//             // button
//             Align(
//               alignment: Alignment(0.9, 0.9), // x antara -1 dan 1, y antara -1 dan 1
//                 child: RaisedButton(
//               child: Text("My Button"),
//               color: Colors.amber,
//               onPressed: () {},
//             ))
//           ],
//         ),
//       ),
//     );
//   }
// }

// (Latihan image widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Image Widget"),
//         ),
//         body: Center(
//           child: Container(
//             color: Colors.black,
//             width: 200,
//             height: 200,
//             padding: EdgeInsets.all(3),
//             child: Image(
//             // menggunakan gambar di internet
//               // image: NetworkImage(
//               //     "https://i.pinimg.com/originals/2a/df/fb/2adffbee6e939b2bd1e32ffa8c763308.jpg"),
//             // menggunakan gambar di folder asset (nama folder dapat diganti asalkan sesuai dengan pengaturan pada pubspec.yaml)
//             image: AssetImage("images/android4.jpg"),
//                   fit: BoxFit.contain,
//                   repeat: ImageRepeat.repeat,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan spacer widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Spacer Widget"),
//         ),
//         body: Center(
//           child: Row(
//             // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               Spacer(flex: 1,),
//               Container(
//                 width: 80,
//                 height: 80,
//                 color: Colors.red,
//               ),
//               Spacer(flex: 2,),
//               Container(
//                 width: 80,
//                 height: 80,
//                 color: Colors.green,
//               ),
//               Spacer(flex: 3,),
//               Container(
//                 width: 80,
//                 height: 80,
//                 color: Colors.blue,
//               ),
//               Spacer(flex: 2,),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan Draggable, DragTarget, SizedBox, Material)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   Color color1 = Colors.red;
//   Color color2 = Colors.amber;
//   Color targetColor;
//   bool isAccepted = false;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan Drag"),
//         ),
//         body: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           children: <Widget>[
//             Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 Draggable<Color>(
//                   data: color1,
//                   child: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: color1,
//                       shape: StadiumBorder(),
//                       elevation: 3,
//                     ),
//                   ),
//                   childWhenDragging: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: Colors.grey,
//                       shape: StadiumBorder(),
//                       elevation: 0,
//                     ),
//                   ),
//                   feedback: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: color1.withOpacity(0.7),
//                       shape: StadiumBorder(),
//                       elevation: 3,
//                     ),
//                   ),
//                 ),
//                 Draggable<Color>(
//                   data: color2,
//                   child: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: color2,
//                       shape: StadiumBorder(),
//                       elevation: 3,
//                     ),
//                   ),
//                   childWhenDragging: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: Colors.grey,
//                       shape: StadiumBorder(),
//                       elevation: 0,
//                     ),
//                   ),
//                   feedback: SizedBox(
//                     width: 50,
//                     height: 50,
//                     child: Material(
//                       color: color2.withOpacity(0.7),
//                       shape: StadiumBorder(),
//                       elevation: 3,
//                     ),
//                   ),
//                 )
//               ],
//             ),
//             DragTarget<Color>(
//             onWillAccept: (value) => true,
//             onAccept: (value) {isAccepted = true; targetColor = value;},
//             builder: (context, candidates, rejected) {
//               return (isAccepted) ? SizedBox(
//                     width: 100,
//                     height: 100,
//                     child: Material(
//                       color: targetColor,
//                       shape: StadiumBorder(),
//                         ),
//                     ):
//                     SizedBox(
//                     width: 100,
//                     height: 100,
//                     child: Material(
//                       color: Colors.black26,
//                       shape: StadiumBorder(),
//                         ),
//                     );
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

// (Latihan 17 => AppBar dengan gradasi)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           leading: Icon(
//             Icons.adb,
//             color: Colors.white,
//           ),
//           title: Text(
//             "AppBar Contoh",
//             style: TextStyle(
//               color: Colors.white,
//             ),
//           ),
//           actions: <Widget>[
//             IconButton(
//               icon: Icon(Icons.settings),
//               onPressed: () {},
//             ),
//             IconButton(
//               icon: Icon(Icons.exit_to_app),
//               onPressed: () {},
//             )
//           ],
//           flexibleSpace: Container(
//             decoration: BoxDecoration(
//               gradient: LinearGradient(
//                 colors: [Color(0xff0096ff), Color(0xff6610f2)],
//                 begin: FractionalOffset.topLeft,
//                 end: FractionalOffset.bottomRight,
//               ),
//               // image: DecorationImage(image: AssetImage("assets/pattern.jpg"), fit: BoxFit.none, repeat: ImageRepeat.repeat)
//               // image: DecorationImage(image: NetworkImage("https://i0.wp.com/www.dalipok.com/wp-content/uploads/2019/09/Wallpaper-dapur-cantik-minimalis-murah-berbagai-gaya.jpg?w=300&h=300&crop=1"), fit: BoxFit.none,repeat: ImageRepeat.repeat)
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 18 => Card Widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         backgroundColor: Colors.green,
//         body: Container(
//           margin: EdgeInsets.all(10),
//           child: ListView(
//             children: <Widget>[
//               buildCard(Icons.account_box, "Account Box"),
//               buildCard(Icons.adb, "Serangga Android")
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Card buildCard(IconData iconData, String text) {
//     return Card(
//       elevation: 5,
//       child: Row(
//         children: <Widget>[
//           Container(margin: EdgeInsets.all(5), child: Icon(iconData, color: Colors.green,)),
//           Text(text)
//         ],
//       ),
//     );
//   }
// }

// (Latihan 19 dan 20 => TextField Widget dan Textfield decoration)
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Text Field"),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              TextField(
                decoration: InputDecoration(
                  suffix: Container(width: 5, height: 5, color: Colors.red,),
                  fillColor: Colors.lightBlue[50],
                  filled: true,
                  // suffix mirip dengan predix, namun berada di belakang
                  icon: Icon(Icons.adb),
                  prefixIcon: Icon(Icons.person),
                  // prefixText: "Name : ",
                  // prefixStyle: TextStyle(color: Colors.blue, fontWeight: FontWeight.w600),
                  labelText: "Nama Lengkap",
                  labelStyle: TextStyle(color: Colors.blue, fontWeight: FontWeight.w600),
                  hintText: "NAMA LENGKAP ANDA",
                  hintStyle: TextStyle(fontSize: 12),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
                ),
                // obscureText: true, // untuk masukkan password
                maxLength: 5, // memaksimalkan karakter
                // maxLines: 2, // memaksimalkan baris
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller,
              ),
              Text(controller.text)
            ],
          ),
        ),
      ),
    );
  }
}
