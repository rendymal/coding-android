import 'package:flutter/material.dart';
// import 'package:flutter_application_31sampai40/post_result_model.dart';
import 'package:flutter_application_31sampai40/user_model.dart'; // post dan get
// import 'dart:ui'; // mengatur font features
// import 'package:audioplayers/audioplayers.dart'; // audioplayer
// import 'package:qrscan/qrscan.dart' as scanner; //qr scan

void main() {
  runApp(MyApp());
}

// (Latihan 31 => gradient opacity/ transparansi bergradasi (shadermask))
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("gradient opacity"),
//         ),
//         body: Center(
//           child: ShaderMask(
//             shaderCallback: (rectangle) {
//               return LinearGradient(
//                       colors: [Colors.black, Colors.transparent],
//                       begin: Alignment.topCenter,
//                       end: Alignment.bottomCenter)
//                   .createShader(
//                       Rect.fromLTRB(0, 0, rectangle.width, rectangle.height));
//             },
//             blendMode: BlendMode.dstIn,
//             child: Image(
//                 width: 300,
//                 image: NetworkImage(
//                     "https://images.fineartamerica.com/images-medium-large-5/seoul-palace-sunset-aaron-s-bedell.jpg")),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 32 => memainkan musik/sound)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }
// class _MyAppState extends State<MyApp> {
//   AudioPlayer audioPlayer;
//   String durasi = "00:00:00";
//   _MyAppState() {
//     audioPlayer = AudioPlayer();
//     audioPlayer.onAudioPositionChanged.listen((duration) {
//       setState(() {
//         durasi = duration.toString();
//       });
//     });
//     audioPlayer.setReleaseMode(ReleaseMode.STOP);
//   }
//   void playSound(String url) async {
//     await audioPlayer.play(url);
//   }
//   void pauseSound() async {
//     await audioPlayer.pause();
//   }
//   void stopSound() async {
//     await audioPlayer.stop();
//   }
//   void resumeSound() async {
//     await audioPlayer.resume();
//   }
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Playing Music"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               RaisedButton(
//                 onPressed: () {
//                   playSound(
//                       // "http://23.237.126.42/ost/top-gear-2-sega-genesis/yzcalloe/01_Title%20Theme.mp3");
//                       "https://www.jango.com/stations/263448190/tunein#");
//                 },
//                 child: Text("Play"),
//               ),
//               RaisedButton(
//                 onPressed: () {
//                   pauseSound();
//                 },
//                 child: Text("Pause"),
//               ),
//               RaisedButton(
//                 onPressed: () {
//                   stopSound();
//                 },
//                 child: Text("Stop"),
//               ),
//               RaisedButton(
//                 onPressed: () {
//                   resumeSound();
//                 },
//                 child: Text("Resume"),
//               ),
//               Text(
//                 durasi,
//                 style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 33 => membaca Qr Scan)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: MainPage(),
//     );
//   }
// }

// class MainPage extends StatefulWidget {
//   @override
//   _MainPageState createState() => _MainPageState();
// }

// class _MainPageState extends State<MainPage> {
//   String text = "Hasil QR Scan";
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("QR Scan"),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             Text(text),
//             SizedBox(
//               height: 20,
//             ),
//             RaisedButton(
//               onPressed: () async {
//                 text = await scanner.scan();
//                 setState(() {});
//               },
//               child: Text("Scan"),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

// (Latihan 34 => font features)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Flutter Typography"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               Text(
//                 "Contoh 01 (Tanpa Apapun)",
//                 style: TextStyle(fontSize: 20),
//               ),
//               Text(
//                 "Contoh 02 (Small Caps)",
//                 style: TextStyle(
//                     fontSize: 20, fontFeatures: [FontFeature.enable('smcp')]),
//               ),
//               Text(
//                 "Contoh 03 dan 1/2 (Small Caps & Frac)",
//                 style: TextStyle(
//                     fontSize: 20, fontFeatures: [FontFeature.enable('smcp'), FontFeature.enable('frac')]),
//               ),
//               Text(
//                 "Contoh 04 (old style)",
//                 style: TextStyle(
//                     fontSize: 20, fontFeatures: [FontFeature.oldstyleFigures()]),
//               ),
//               Text(
//                 "Contoh 05 (Default)",
//                 style: TextStyle(
//                     fontSize: 20, fontFeatures: [FontFeature.oldstyleFigures()]),
//               ),
//               Text(
//                 "Contoh 06 (Style set nomor 5)",
//                 style: TextStyle(
//                     fontSize: 20, fontFeatures: [FontFeature.stylisticSet(5)]),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 35 => clippath)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(title: Text("Custom Clipper"),),
//         body: Center(
//           child: ClipPath(
//             clipper: MyClipper(),
//             child: Image(
//               width: 300,
//               image: NetworkImage("https://images.fineartamerica.com/images-medium-large-5/seoul-palace-sunset-aaron-s-bedell.jpg")),
//           ),
//         ),
//       ),
//     );
//   }
// }
// class MyClipper extends CustomClipper<Path>{
//   @override
//   Path getClip(Size size) {
//     Path path = Path();
//     path.lineTo(0, size.height); // garis kebawah
//     path.quadraticBezierTo(size.width / 2, size.height *0.75, size.width, size.height); // garis bawah yang melengkung
//     path.lineTo(size.width, 0); //garis ke bawah
//     path.close();
//     return path;
//   }
//   @override
//   bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
// }

// (Latihan 36 => HTTP Request / Koneksi ke API (POST Method))
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   // ignore: avoid_init_to_null
//   PostResult postResult = null;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("API Demo"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               Text((postResult != null)
//                   ? postResult.id +
//                       " | " +
//                       postResult.name +
//                       " | " +
//                       postResult.job +
//                       " | " +
//                       postResult.created
//                   : "Tidak ada Data"),
//               RaisedButton(
//                 onPressed: () {
//                   PostResult.connecToAPI("RENDY", "MAHASISWA").then((value) {
//                     postResult = value;
//                     setState(() {});
//                   });
//                 },
//                 child: Text("POST"),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 37 => HTTP Request / Koneksi ke API (GET Method))
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }
// class _MyAppState extends State<MyApp> {
//   // ignore: avoid_init_to_null
//   PostResult postResult = null;
//   // ignore: avoid_init_to_null
//   User user = null;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("API Demo"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               Text((user != null)
//                   ? user.id + " | " + user.name
//                   : "Tidak ada Data"),
//               RaisedButton(
//                 onPressed: () {
//                   User.connectToAPI("7").then((value) {
//                     user = value;
//                     setState(() {});
//                   });
//                 },
//                 child: Text('GET'),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 38 => HTTP Request / Koneksi ke API (Kumpulan Data))
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String outPut = "no data";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("API Demo"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(outPut),
              // ignore: deprecated_member_use
              RaisedButton(
                onPressed: () {
                  User.getUsers("1").then((users) {
                    outPut = "";
                    for (int i = 0; i < users.length; i++)
                      outPut = outPut + "[ " + users[i].name + " ]";
                    setState(() {});
                  });
                },
                child: Text("GET"),
              )
            ],
          ),
        ),
      ),
    );
  }
}

// (Latihan 39 => Switch dan AnimatedSwitcher Widget)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   bool isOn = false;
//   Widget myWidget = Container(
//     width: 200,
//     height: 100,
//     decoration: BoxDecoration(
//         color: Colors.red, border: Border.all(color: Colors.black, width: 3)),
//   );
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Animated Switcher"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             children: [
//               AnimatedSwitcher(
//                 child: myWidget,
//                 duration: Duration(seconds: 1),
//                 transitionBuilder: (child, animation) => ScaleTransition(
//                   scale: animation,
//                   child: child,
//                 ),
//               ),
//               Switch(
//                   activeColor: Colors.green,
//                   inactiveThumbColor: Colors.red,
//                   inactiveTrackColor: Colors.red[200],
//                   value: isOn,
//                   onChanged: (newValue) {
//                     isOn = newValue;
//                     setState(() {
//                       if (isOn == true)
//                         // myWidget = SizedBox(
//                         //   width: 200,
//                         //   height: 100,
//                         //   child: Center(
//                         //     child: Text(
//                         //       "Switch : On",
//                         //       style: TextStyle(
//                         //           color: Colors.green,
//                         //           fontWeight: FontWeight.w700,
//                         //           fontSize: 20),
//                         //     ),
//                         //   ),
//                         // );
//                         myWidget = Container(
//                           key: ValueKey(1), // tanpa key, maak container tidak terswitch
//                           width: 200,
//                           height: 100,
//                           decoration: BoxDecoration(
//                               color: Colors.green,
//                               border:
//                                   Border.all(color: Colors.black, width: 3)),
//                         );
//                       else
//                         myWidget = Container(
//                           key: ValueKey(2),
//                           width: 200,
//                           height: 100,
//                           decoration: BoxDecoration(
//                               color: Colors.red,
//                               border:
//                                   Border.all(color: Colors.black, width: 3)),
//                         );
//                     });
//                   }),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (Latihan 40 => AnimatedPadding Widget)
// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {
//   double myPadding = 1;
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Animated Padding"),
//         ),
//         body: Column(
//           children: [
//             Flexible(
//               child: Row(
//                 children: [
//                   Flexible(
//                     child: AnimatedPadding(
//                       duration: Duration(seconds: 1),
//                       padding: EdgeInsets.all(myPadding),
//                       child: GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             myPadding = myPadding + 20;
//                           });
//                         },
//                         child: Container(
//                           color: Colors.red,
//                         ),
//                       ),
//                     ),
//                     flex: 1,
//                   ),
//                   Flexible(
//                     child: AnimatedPadding(
//                       duration: Duration(seconds: 1),
//                       padding: EdgeInsets.all(myPadding),
//                       child: GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             myPadding = myPadding * 3;
//                           });
//                         },
//                         child: Container(
//                           color: Colors.green,
//                         ),
//                       ),
//                     ),
//                     flex: 1,
//                   )
//                 ],
//               ),
//               flex: 1,
//             ),
//             Flexible(
//               child: Row(
//                 children: [
//                   Flexible(
//                     child: AnimatedPadding(
//                       duration: Duration(seconds: 1),
//                       padding: EdgeInsets.all(myPadding),
//                       child: GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             myPadding = myPadding + 3;
//                           });
//                         },
//                         child: Container(
//                           color: Colors.blue,
//                         ),
//                       ),
//                     ),
//                     flex: 1,
//                   ),
//                   Flexible(
//                     child: AnimatedPadding(
//                       duration: Duration(seconds: 1),
//                       padding: EdgeInsets.all(myPadding),
//                       child: GestureDetector(
//                         onTap: () {
//                           setState(() {
//                             myPadding = myPadding / 10;
//                           });
//                         },
//                         child: Container(
//                           color: Colors.yellow,
//                         ),
//                       ),
//                     ),
//                     flex: 1,
//                   )
//                 ],
//               ),
//               flex: 1,
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
