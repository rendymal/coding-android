import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:flutter_application_21sampai30/colorful_button.dart';
// import 'package:qr_flutter/qr_flutter.dart';

void main() {
  runApp(MyApp());
}

// membuat orientasi tetap potrait
// void main() {
//   SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_){
//     runApp(MyApp());
//   });

// }

// (Latihan 21 => Media Query)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MainPage(),
//     );
//   }
// }
// class MainPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan Media Query"),
//         ),
//         body: (MediaQuery.of(context).orientation == Orientation.portrait)
//             ? Column(
//                 children: generateContainer(),
//               )
//             : Row(
//                 children: generateContainer(),
//               ), // jika orientasi layar adalah potrait, maka column dan jika tidak maka row
//         // Container(
//         //   color: Colors.red,
//         //   width: MediaQuery.of(context).size.width / 3,
//         //   height: MediaQuery.of(context).size.height / 2,
//         // ),
//         );
//   }
//   List<Widget> generateContainer() {
//     return [
//       Container(
//         color: Colors.red,
//         width: 100,
//         height: 100,
//       ),
//       Container(
//         color: Colors.green,
//         width: 100,
//         height: 100,
//       ),
//       Container(
//         color: Colors.blue,
//         width: 100,
//         height: 100,
//       ),
//     ];
//   }
// }

// (latihan 22 => inkwell (membuat sendiri button gradiasi))
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Latihan button"),
//         ),
//         body: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             children: [
//               RaisedButton(
//                 onPressed: () {},
//                 color: Colors.amber,
//                 child: Text("Raised Button"),
//                 shape: StadiumBorder(),
//               ),
//               Material(
//                 borderRadius: BorderRadius.circular(20),
//                 elevation: 2,
//                 child: Container(
//                   width: 150,
//                   height: 40,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(20),
//                     gradient: LinearGradient(
//                         colors: [Colors.purple, Colors.pink],
//                         begin: Alignment.topCenter,
//                         end: Alignment.bottomCenter),
//                   ),
//                   child: Material(
//                     borderRadius: BorderRadius.circular(20),
//                     color: Colors.transparent,
//                     child: InkWell(
//                       splashColor: Colors.amber,
//                       borderRadius: BorderRadius.circular(20),
//                       onTap: () {},
//                       child: Center(
//                         child: Text(
//                           "My Button",
//                           style: TextStyle(
//                               color: Colors.black, fontWeight: FontWeight.w600),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// (latihan 23 => Opacity (custom card dengan latar bercorak))
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MainPage(),
//     );
//   }
// }
// class MainPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "Custom Card Example",
//           style: TextStyle(
//             color: Colors.white,
//           ),
//         ),
//         backgroundColor: Color(0xFF8C062F),
//       ),
//       body: Stack(
//         children: [
//           Container(
//             decoration: BoxDecoration(
//                 gradient: LinearGradient(
//               colors: [Color(0xFFFE5788), Color(0xFFF56D5D)],
//               begin: Alignment.topCenter,
//               end: Alignment.bottomCenter,
//             )),
//           ),
//           Center(
//             child: SizedBox(
//               width: MediaQuery.of(context).size.width * 0.8,
//               height: MediaQuery.of(context).size.height * 0.7,
//               child: Card(
//                 elevation: 10,
//                 child: Stack(
//                   children: [
//                     Opacity(
//                       opacity: 0.7,
//                       child: Container(
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(4),
//                           image: DecorationImage(
//                               image: NetworkImage(
//                                   "https://www.toptal.com/designers/subtlepatterns/patterns/memphis-mini.png"),
//                               fit: BoxFit.cover),
//                         ),
//                       ),
//                     ),
//                     Container(
//                       height: MediaQuery.of(context).size.height * 0.35,
//                       decoration: BoxDecoration(
//                         borderRadius: BorderRadius.only(
//                             topLeft: Radius.circular(4),
//                             topRight: Radius.circular(4)),
//                         image: DecorationImage(
//                             image: NetworkImage(
//                                 "https://cdn.pixabay.com/photo/2018/01/20/08/33/sunset-3094078_960_720.jpg"),
//                             fit: BoxFit.cover),
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.fromLTRB(
//                           20,
//                           50 + MediaQuery.of(context).size.height * 0.35,
//                           20,
//                           20),
//                       child: Center(
//                         child: Column(
//                           children: [
//                             Text(
//                               "Beautiful Sunset at Paddy Field",
//                               style: TextStyle(
//                                   color: Color(0xFFF56D5D), fontSize: 25),
//                               maxLines: 2,
//                               textAlign: TextAlign.center,
//                             ),
//                             Container(
//                               margin: EdgeInsets.fromLTRB(0, 20, 0, 15),
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.center,
//                                 children: [
//                                   Text(
//                                     "Posted On ",
//                                     style: TextStyle(
//                                         color: Colors.grey, fontSize: 12),
//                                   ),
//                                   Text(
//                                     "June 18, 2018",
//                                     style: TextStyle(
//                                         color: Color(0xFFF56D5D), fontSize: 12),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             Row(
//                               children: [
//                                 Spacer(flex: 10,),
//                                 // Icon jempol
//                                 Icon(Icons.thumb_up, size: 18, color: Colors.grey),
//                                 Spacer(flex: 1,),
//                                 // Text
//                                 Text("99", style: TextStyle(color: Colors.grey,),),
//                                 Spacer(flex: 5,),
//                                 // Icon
//                                 Icon(Icons.comment, size: 18, color: Colors.grey),
//                                 Spacer(flex: 1,),
//                                 // Text
//                                 Text("1000", style: TextStyle(color: Colors.grey,),),
//                                 Spacer(flex: 10,)
//                               ],
//                             )
//                           ],
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

// (latihan 24 => Positioned, FLoatingActionButton, LoginPage)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
// //       debugShowCheckedModeBanner: false, //menghilangkan banner debug
//       home: MainPage(),
//     );
//   }
// }
// class MainPage extends StatelessWidget {
//   double getSmallDiameter(BuildContext context) =>
//       MediaQuery.of(context).size.width * 2 / 3;
//   double getBigDiameter(BuildContext context) =>
//       MediaQuery.of(context).size.width * 7 / 8;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Color(0xFFEEEEEE),
//       body: Stack(
//         children: [
//           Positioned(
//             right: -getSmallDiameter(context) / 3,
//             top: -getSmallDiameter(context) / 3,
//             child: Container(
//               width: getSmallDiameter(context),
//               height: getSmallDiameter(context),
//               decoration: BoxDecoration(
//                 shape: BoxShape.circle,
//                 gradient: LinearGradient(
//                     colors: [Color(0xFF006666), Color(0xFF666666)],
//                     begin: Alignment.topCenter,
//                     end: Alignment.bottomCenter),
//               ),
//             ),
//           ),
//           Positioned(
//             left: -getBigDiameter(context) / 4,
//             top: -getBigDiameter(context) / 4,
//             child: Container(
//               child: Center(
//                 child: Text(
//                   "dribblee",
//                   style: TextStyle(
//                     fontSize: 30,
//                     color: Colors.white,
//                   ),
//                 ),
//               ),
//               width: getBigDiameter(context),
//               height: getBigDiameter(context),
//               decoration: BoxDecoration(
//                 shape: BoxShape.circle,
//                 gradient: LinearGradient(
//                     colors: [Color(0xFFCC6666), Color(0xFFCC6699)],
//                     begin: Alignment.topCenter,
//                     end: Alignment.bottomCenter),
//               ),
//             ),
//           ),
//           Positioned(
//             right: -getBigDiameter(context) / 2,
//             bottom: -getBigDiameter(context) / 2,
//             child: Container(
//               width: getBigDiameter(context),
//               height: getBigDiameter(context),
//               decoration: BoxDecoration(
//                 shape: BoxShape.circle,
//                 color: Color(0xFFF3E9EE),
//               ),
//             ),
//           ),
//           Align(
//             alignment: Alignment.bottomCenter,
//             child: ListView(
//               children: [
//                 Container(
//                   decoration: BoxDecoration(
//                       color: Colors.white,
//                       border: Border.all(
//                         color: Colors.grey,
//                       ),
//                       borderRadius: BorderRadius.circular(5)),
//                   margin: EdgeInsets.fromLTRB(20, 350, 20, 10),
//                   padding: EdgeInsets.fromLTRB(10, 0, 10, 25),
//                   child: Column(
//                     children: [
//                       TextField(
//                         decoration: InputDecoration(
//                             icon: Icon(
//                               Icons.email,
//                               color: Color(0xFFFf4891),
//                             ),
//                             focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(
//                               color: Color(0xFFFf4891),
//                             )),
//                             labelText: "Email: ",
//                             labelStyle: TextStyle(
//                               color: Color(0xFFFf4891),
//                             )),
//                       ),
//                       TextField(
//                         obscureText: true,
//                         decoration: InputDecoration(
//                             icon: Icon(
//                               Icons.vpn_key,
//                               color: Color(0xFFFf4891),
//                             ),
//                             focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(
//                               color: Color(0xFFFf4891),
//                             )),
//                             labelText: "Password: ",
//                             labelStyle: TextStyle(
//                               color: Color(0xFFFf4891),
//                             )),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Align(
//                     alignment: Alignment.centerRight,
//                     child: Container(
//                       margin: EdgeInsets.fromLTRB(0, 0, 20, 20),
//                       child: Text(
//                         "Forgot Password ?",
//                         style: TextStyle(
//                           color: Color(0xFFFf4891),
//                           fontSize: 11,
//                         ),
//                       ),
//                     )),
//                 Container(
//                   margin: EdgeInsets.fromLTRB(20, 0, 20, 30),
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       SizedBox(
//                         width: MediaQuery.of(context).size.width / 2,
//                         height: 40,
//                         child: Container(
//                           child: Material(
//                             borderRadius: BorderRadius.circular(20),
//                             color: Colors.transparent,
//                             child: InkWell(
//                               borderRadius: BorderRadius.circular(20),
//                               splashColor: Colors.amber,
//                               onTap: () {},
//                               child: Center(
//                                 child: Text(
//                                   "SIGN IN",
//                                   style: TextStyle(
//                                     color: Colors.white,
//                                     fontWeight: FontWeight.w700,
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ),
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(20),
//                             gradient: LinearGradient(
//                               colors: [Color(0xFFB226B2), Color(0xFFFF4891)],
//                               begin: Alignment.topCenter,
//                               end: Alignment.bottomCenter,
//                             ),
//                           ),
//                         ),
//                       ),
//                       FloatingActionButton(
//                         onPressed: () {},
//                         elevation: 0,
//                         mini: true,
//                         child: Image(
//                             image: NetworkImage(
//                                 "https://1000logos.net/wp-content/uploads/2016/11/Facebook-logo.png")),
//                       ),
//                       FloatingActionButton(
//                         onPressed: () {},
//                         elevation: 0,
//                         mini: true,
//                         child: Image(
//                             image: NetworkImage(
//                                 "https://assets.stickpng.com/images/580b57fcd9996e24bc43c53e.png")),
//                       )
//                     ],
//                   ),
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Text(
//                       "DON'T HAVE AN ACCOUNT? ",
//                       style: TextStyle(
//                           fontSize: 11,
//                           color: Colors.grey,
//                           fontWeight: FontWeight.w500),
//                     ),
//                     Text(
//                       "SIGN UP",
//                       style: TextStyle(
//                           fontSize: 11,
//                           color: Color(0xFFFF4891),
//                           fontWeight: FontWeight.w700),
//                     )
//                   ],
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

// (latihan 25 => Hero dan CLipRRect Widget)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: MainPage(),
//     );
//   }
// }
// class MainPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.lightBlue[50],
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           "Latihan Hero Animation",
//           style: TextStyle(
//             color: Colors.white,
//           ),
//         ),
//       ),
//       body: GestureDetector(
//         onTap: () {
//           Navigator.push(context, MaterialPageRoute(builder: (context) {
//             return SecondPage();
//           }));
//         },
//         child: Hero(
//           tag: 'pp',
//           child: ClipRRect(
//             borderRadius: BorderRadius.circular(50),
//             child: Container(
//               width: 100,
//               height: 100,
//               child: Image(
//                 fit: BoxFit.cover,
//                 image: NetworkImage(
//                     "https://inikpop.com/wp-content/uploads/2018/12/jennie.jpg"),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
// class SecondPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.pink[50],
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           "Latihan Hero Animation",
//           style: TextStyle(
//             color: Colors.white,
//           ),
//         ),
//       ),
//       body: Center(
//         child: Hero(
//           tag: 'pp',
//           child: ClipRRect(
//             borderRadius: BorderRadius.circular(100),
//             child: Container(
//               width: 200,
//               height: 200,
//               child: Image(
//                 fit: BoxFit.cover,
//                 image: NetworkImage(
//                     "https://inikpop.com/wp-content/uploads/2018/12/jennie.jpg"),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (latihan 26 => AppBar dengan custom height (PreferredSized))
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: PreferredSize(
//           preferredSize: Size.fromHeight(200),
//           child: AppBar(
//             backgroundColor: Colors.amber,
//             flexibleSpace: Positioned(
//               right: 0,
//               bottom: 0,
//               child: Container(
//                 margin: EdgeInsets.all(20),
//                 child: Text(
//                   "AppBar With Custom Height",
//                   style: TextStyle(
//                       fontSize: 20,
//                       color: Colors.white,
//                       fontWeight: FontWeight.w700),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// (latihan 27 dan 28=> TabBar Custom)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     TabBar myTabBar = TabBar(
//         // indicatorColor: Colors.red, // indikator garis
//         indicator: BoxDecoration(
//             color: Colors.red,
//             border: Border(
//               top: BorderSide(
//                 color: Colors.purple,
//                 width: 5
//               ),
//             )),
//         tabs: <Widget>[
//           Tab(
//             icon: Icon(Icons.comment),
//             text: "Comments",
//           ),
//           Tab(
//             icon: Icon(Icons.computer),
//             text: "Computer",
//           )
//         ]);
//     return MaterialApp(
//       home: DefaultTabController(
//           length: 2,
//           child: Scaffold(
//             appBar: AppBar(
//               title: Text("Contoh Tab Bar"),
//               bottom: PreferredSize(
//                   preferredSize: Size.fromHeight(myTabBar.preferredSize.height),
//                   child: Container(color: Colors.amber, child: myTabBar)),
//             ),
//             body: TabBarView(children: <Widget>[
//               Center(
//                 child: Text("Rendy Malikulmulki Wahid, Tab Pertama"),
//               ),
//               Center(
//                 child: Text("Rendy Malikulmulki Wahid, Tab Kedua"),
//               )
//             ]),
//           )),
//     );
//   }
// }


// (latihan 29=> Menampilkan QR COde)
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         body: Center(
//           child: QrImage(
//             version: 6,
//             backgroundColor: Colors.grey,
//             foregroundColor: Colors.black,
//             errorCorrectionLevel: QrErrorCorrectLevel.M,
//             padding: EdgeInsets.all(30),
//             size: 300,
//             data: "https://fisika.mipa.unsri.ac.id/Sistem_monitoring",
//           ),
//         ),
//       ),
//     );
//   }
// }

// (latihan 30=> Button Belah Ketupat Warna Warni (Transform))
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: Text("Colorfull Buttons"),),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ColorfulButton(Colors.pink, Colors.blue, Icons.adb),
              ColorfulButton(Colors.amber, Colors.black, Icons.computer),
              ColorfulButton(Colors.purple, Colors.green, Icons.comment),
              ColorfulButton(Colors.blue, Colors.yellow, Icons.contact_phone),
            ],
          ),
        ),
      ),
    );
  }
}